from django.apps import AppConfig


class PersonaHerenciaConfig(AppConfig):
    name = 'persona_herencia'

from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    # path('api/', include('persona.urls')),
    path('api/', include(('persona_abstract.urls', 'personal'), namespace='personas')),
]

from rest_framework import viewsets, mixins
from persona.serializers import *
from persona.models import *


class PersonaViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin,
                     viewsets.GenericViewSet):
    queryset = Persona.objects.all()
    serializer_class = PersonaSerializer


class PersonaFisicaViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin,
                     viewsets.GenericViewSet):
    queryset = PersonaFisica.objects.all()
    serializer_class = PersonaFisicaSerializer


class PersonaJuridicaViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin,
                     viewsets.GenericViewSet):
    queryset = PersonaJuridica.objects.all()
    serializer_class = PersonaJuricaSerializer
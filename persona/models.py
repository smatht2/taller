from django.db import models
from django.contrib.auth.models import User


class Persona(models.Model):
    domicilio = models.CharField(max_length=150)


# flied dominio

class PersonaJuridica(models.Model):
    TIPO = [
        ('PR', 'Privado'),
        ('PU', 'Público')
    ]
    cuit = models.CharField(max_length=22)
    persona = models.ForeignKey(
        Persona,
        on_delete=models.CASCADE,
        related_name="juridicas"
    )
    tipo = models.CharField(
        max_length=2,
        choices=TIPO
    )


class PersonaFisica(models.Model):
    apellido = models.CharField(max_length=100)
    cuil = models.CharField(max_length=20)
    dni = models.CharField(max_length=20)
    nombre = models.CharField(max_length=100)
    persona = models.ForeignKey(
        Persona,
        on_delete=models.CASCADE,
        related_name="fisicas"
    )


class Parte(models.Model):
    persona_fisica = models.ForeignKey(
        PersonaFisica,
        on_delete=models.CASCADE,
        related_name="partes"
    )


class Profesional(models.Model):
    TIPO_PROFESION = [
        ('ABO', 'Abogado'),
        ('ESC', 'Escribano')
    ]
    matricula = models.CharField(
        max_length=20, blank=True, null=True)
    persona_fisica = models.ForeignKey(
        PersonaFisica,
        on_delete=models.CASCADE,
        related_name="profesionales"
    )
    tipo = models.CharField(
        max_length=3,
        choices=TIPO_PROFESION
    )


class Representante(models.Model):
    # OneToOne suponiendo parte - representate = 1 causa - representante/s
    parte = models.OneToOneField(
        Parte,
        on_delete=models.CASCADE,
        related_name="representantes"
    )
    profesional = models.ManyToManyField(
        Profesional, related_name="representados")


class Usuario(models.Model):
    persona = models.OneToOneField(
        Persona,
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )
    user_django = models.OneToOneField(
        User,
        on_delete=models.CASCADE
    )

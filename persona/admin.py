from django.contrib import admin
from persona.models import *


@admin.register(Persona)
class PersonaAdmin(admin.ModelAdmin):
    pass


@admin.register(PersonaJuridica)
class PersonaJuridicaAdmin(admin.ModelAdmin):
    pass


@admin.register(PersonaFisica)
class PersonaFisicaAdmin(admin.ModelAdmin):
    pass


@admin.register(Parte)
class ParteAdmin(admin.ModelAdmin):
    pass


@admin.register(Profesional)
class ProfesionalAdmin(admin.ModelAdmin):
    pass


@admin.register(Representante)
class RepresentanteAdmin(admin.ModelAdmin):
    pass


@admin.register(Usuario)
class UsuarioAdmin(admin.ModelAdmin):
    pass

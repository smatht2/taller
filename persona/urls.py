from rest_framework.routers import DefaultRouter, SimpleRouter
from django.conf import settings
from persona.views import *
from django.urls import path, include


if settings.DEBUG:
    router = SimpleRouter()
else:
    router = DefaultRouter()

router.register('personas/fisicas', PersonaFisicaViewSet, basename='personas_fisicas')
router.register('personas/juridicas', PersonaJuridicaViewSet, basename='personas_juridicas')
router.register('personas', PersonaViewSet, basename='personas')


urlpatterns = [
      path('', include((router.urls, 'api'), namespace='api'))
]
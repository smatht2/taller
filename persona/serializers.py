from rest_framework.serializers import ModelSerializer, SerializerMethodField
from persona.models import *


class PersonaSerializer(ModelSerializer):

    class Meta:
        model = Persona
        fields = '__all__'


class PersonaFisicaSerializer(ModelSerializer):

    class Meta:
        model = PersonaFisica
        fields = '__all__'


class PersonaJuricaSerializer(ModelSerializer):
    tipo = SerializerMethodField()

    class Meta:
        model = PersonaJuridica
        fields = '__all__'
    
    def get_tipo(self,obj):
        return obj.get_tipo_display()



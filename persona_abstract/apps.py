from django.apps import AppConfig


class PersonaAbstractConfig(AppConfig):
    name = 'persona_abstract'
    verbose_name = 'Persona Abstracta'

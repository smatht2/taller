from django.shortcuts import render
from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet

from persona_abstract.models import Parte, Profesional, Usuario
from persona_abstract.serializers import PartesSerializer, ProfesionalesSerializer, UsuariosSerializer


class PartesViewSet(mixins.ListModelMixin,
                    mixins.CreateModelMixin,
                    mixins.UpdateModelMixin,
                    mixins.RetrieveModelMixin,
                    mixins.DestroyModelMixin,
                    GenericViewSet):

    queryset = Parte.objects.all()
    serializer_class = PartesSerializer


class ProfesionalesViewSet(mixins.ListModelMixin,
                           mixins.CreateModelMixin,
                           mixins.UpdateModelMixin,
                           mixins.RetrieveModelMixin,
                           mixins.DestroyModelMixin,
                           GenericViewSet):

    queryset = Profesional.objects.all()
    serializer_class = ProfesionalesSerializer


class UsuariosViewSet(mixins.ListModelMixin,
                      mixins.CreateModelMixin,
                      mixins.UpdateModelMixin,
                      mixins.RetrieveModelMixin,
                      mixins.DestroyModelMixin,
                      GenericViewSet):

    queryset = Usuario.objects.all()
    serializer_class = UsuariosSerializer

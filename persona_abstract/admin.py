from django.contrib import admin

from persona_abstract.models import Parte, Profesional, Usuario


@admin.register(Parte)
class ParteAdmin(admin.ModelAdmin):
    list_display = ('per_id', 'parte_id', 'tipo', 'cuit')
    list_filter = ('tipo',)
    search_fields = ('cuit',)
    readonly_fields = ('per_id',)
    exclude = ('InsertedOn', 'UpdatedOn', 'DeletedOn')


@admin.register(Profesional)
class ProfesionalAdmin(admin.ModelAdmin):
    list_display = ('per_id', 'profesional_id', 'tipo', 'matricula')
    list_filter = ('tipo',)
    search_fields = ('cuit',)
    readonly_fields = ('per_id',)
    exclude = ('InsertedOn', 'UpdatedOn', 'DeletedOn')


@admin.register(Usuario)
class ProfesionalAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'apellido')
    readonly_fields = ('per_id',)
    exclude = ('InsertedOn', 'UpdatedOn', 'DeletedOn')

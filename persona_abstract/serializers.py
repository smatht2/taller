from rest_framework import serializers

from persona_abstract.models import Parte, Profesional, Usuario


class PartesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Parte
        fields = '__all__'


class ProfesionalesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profesional
        fields = '__all__'


class UsuariosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Usuario
        fields = '__all__'

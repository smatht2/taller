from django.urls import path, include

from rest_framework.routers import DefaultRouter

from persona_abstract.views import PartesViewSet, ProfesionalesViewSet, UsuariosViewSet

router = DefaultRouter()
router.register(r'partes', PartesViewSet, basename='partes')
router.register(r'profesionales', ProfesionalesViewSet, basename='partes')
router.register(r'usuarios', UsuariosViewSet, basename='partes')

urlpatterns = [
    path('', include(router.urls))
]

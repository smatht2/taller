from django.db import models

from persona_abstract.models.persona_fisica import PersonaFisica


class Parte(PersonaFisica):
    parte_id = models.AutoField(primary_key=True)
    TIPO = [
        ('PR', 'Privado'),
        ('PU', 'Público')
    ]
    tipo = models.CharField(
        max_length=2,
        choices=TIPO
    )
    cuit = models.CharField(max_length=22)

    class Meta:
        db_table = 'parte'
        verbose_name = 'Parte'
        verbose_name_plural = 'Partes'

from django.db import models
from django.contrib.auth.models import User

from persona_abstract.models.persona_fisica import PersonaFisica


class Usuario(PersonaFisica):
    user_django = models.OneToOneField(
        User,
        on_delete=models.CASCADE
    )

    class Meta:
        db_table = 'usuario'
        verbose_name = 'Usuario'
        verbose_name_plural = 'Usuarios'

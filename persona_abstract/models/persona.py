import uuid

from django.db import models


class Persona(models.Model):
    per_id = models.UUIDField(default=uuid.uuid4)
    domicilio = models.CharField(max_length=150)
    InsertedOn = models.DateTimeField(
        auto_now_add=True,
        blank=True,
        null=True
    )
    UpdatedOn = models.DateTimeField(
        blank=True,
        null=True
    )
    DeletedOn = models.DateTimeField(
        blank=True,
        null=True
    )

    class Meta:
        abstract = True

        ordering = ['-InsertedOn', '-UpdatedOn']

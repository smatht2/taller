from django.db import models

from persona_abstract.models.persona_fisica import PersonaFisica


class Profesional(PersonaFisica):
    profesional_id = models.AutoField(primary_key=True)
    TIPO_PROFESION = [
        ('ABO', 'Abogado'),
        ('ESC', 'Escribano')
    ]
    tipo = models.CharField(
        max_length=3,
        choices=TIPO_PROFESION
    )
    matricula = models.CharField(
        max_length=20, blank=True, null=True)

    class Meta:
        db_table = 'profesional'
        verbose_name = 'Profesional'
        verbose_name_plural = 'Profesionales'

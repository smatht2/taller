from django.db import models

from persona_abstract.models import Persona


class PersonaFisica(Persona):
    nombre = models.CharField(max_length=100)
    apellido = models.CharField(max_length=100)

    class Meta:
        abstract = True
